﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _roadWidth = 1f;
    [SerializeField] private float _speed = 1f;
    [SerializeField] private InputHandler _inputHandler = null;
    [SerializeField] private AnimationController _animation = null;
    [SerializeField] private GameObject _camera = null;
    private  bool Logic = true;

    void Start()
    {
        _animation.SetTrigger("IsRun");
    }

    void Update()
    {
        if (Logic == true)
        {
            Move();
        }
    }

    private void OnTriggerEnter()
    {
        Debug.Log("Столконовение");
        Die();
    }

    private void Move ()
    {

        Vector3 horizontalOffset = _inputHandler.OffsetX * _roadWidth * Vector3.right;

        float resultOffset = transform.position.x + horizontalOffset.x;
        if (Mathf.Abs(resultOffset) > _roadWidth / 2f)
        {
            horizontalOffset.x = 0f;
        }
        
        Vector3 forwardOffset = _speed * Time.deltaTime * Vector3.forward;
        _camera.transform.Translate(forwardOffset);
        transform.Translate(horizontalOffset + forwardOffset);

    }

    private void Die()
    {
        Logic = false;
        _animation.SetTrigger("IsDead");
    }
}
