﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    private Vector3 _pozitionSvipe;
    private float _offset;

    public float OffsetX => _offset;

    // Update is called once per frame
    void Update()
    {
        Svipe(); 
    }
    private void Svipe()
    {
        if(Input.GetMouseButtonDown(0))
        {
            _pozitionSvipe = Input.mousePosition;
        }

        if(Input.GetMouseButton(0))
        {
            _offset = (Input.mousePosition -_pozitionSvipe).x;
            _offset /= Screen.width;
            _pozitionSvipe = Input.mousePosition;

        }

        if (Input.GetMouseButtonUp(0)) 
        {
            _offset = 0;
        }

    }


}
