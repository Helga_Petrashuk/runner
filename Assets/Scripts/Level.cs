﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] private GameObject _roadPrefab = null;
    [SerializeField] private float _roadWidht = 6f;

    [SerializeField] private float _maxDistanceOffset = 6f;
    [SerializeField] private float _minDistanceOffset = 3f;

    [SerializeField] private int _obstacleCountLine = 3;
    
    
    [SerializeField] private GameObject _obstaclePrefab = null;
    [SerializeField] private Transform _startPoint= null;
    [SerializeField] private int _startSegmentCount = 10;

    //private int _startSeed = 25;
    private Vector3 _lastPoint;
    private void Start()
    {
        GenerateRoad();
        GenerateObstacle();
        
        
        //if (PlayerPrefs.HasKey("SEED"))
        //if (PlayerPrefs.HasKey("SEED"))
        //{
        //    Random.InitState(PlayerPrefs.GetInt("SEED"));
        //}
        //else
        //{
        //    Random.InitState(_startSeed);
        //}

       // int seed = PlayerPrefs.GetInt("SEED", _startSeed);
       //Debug.Log(seed);
        //Random.InitState(seed);
        //seed++;
        //PlayerPrefs.GetInt("SEED", seed);


    }
    public void GenerateRoad ()
    {
        Vector3 generationPozition = _startPoint.position;
        for (int i = 0; i <= _startSegmentCount; i++)
        {
            GameObject newPlatform = Instantiate(_roadPrefab);
            newPlatform.transform.position = generationPozition;
            newPlatform.transform.SetParent(transform);

            generationPozition = newPlatform.GetComponentInChildren<RoadSegment>().NextPosizion;
            newPlatform.GetComponentInChildren<RoadSegment>().OnInvisible += SegmentReplacer;
            if (i==_startSegmentCount-1)
            {
                _lastPoint = generationPozition;
            }
        }
    }

    

    private void GenerateObstacle()
    {
        float roadLenght = Vector3.Distance(_startPoint.position, _lastPoint);
        float currenuLenght = 5f;

        while(currenuLenght<roadLenght)
        {
            int obstacleCount = Random.Range(1, _obstacleCountLine);
            List<int> places = new List<int>();
            
            for(int i= 0; i < _obstacleCountLine; i++)
            {
                places.Add(i);
            }

            List<int> usedPlase = new List<int>();

            while (usedPlase.Count < obstacleCount)
            {
                int rIndex = Random.Range(0, places.Count);

                usedPlase.Add(places[rIndex]);
                places.RemoveAt(rIndex);
            }

            GenerateObstacleLine(usedPlase, currenuLenght);

            currenuLenght += Random.Range(_minDistanceOffset, _maxDistanceOffset);
        }

    }

    private void GenerateObstacleLine (List <int>  plases, float roadLength)
    {
        float startPointX = -_roadWidht / 2f;
        float offetX = _roadWidht / _obstacleCountLine;

        for (int i=0; i<plases.Count; i++)
        {
            Vector3 position = new Vector3(startPointX + plases[i] * offetX, 0f, roadLength);
            Instantiate(_obstaclePrefab, position, Quaternion.identity, transform);
        }
    }

    private void SegmentReplacer(RoadSegment roadSegment)
    {
        roadSegment.ParentPosition = _lastPoint;
        _lastPoint = roadSegment.NextPosizion;
    }
}
