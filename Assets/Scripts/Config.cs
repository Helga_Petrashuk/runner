﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Config", menuName ="Configs/Main Config")]
public class Config : ScriptableObject
{

    public float Speed = 0f;
    public float RoadWidth = 0f;
}
